# Fifo

Simple practice exercise creating an Abstract Data Type with Elixir. The exercise
was specifically directed towards learning how to create, update, edit, and delete
elements from an embedded linked list using a Functional Programming approach.

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/fifo](https://hexdocs.pm/fifo).

