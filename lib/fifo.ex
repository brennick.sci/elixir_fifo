defmodule Fifo do
  @moduledoc"""
  representation of a simple FIFO linked list with bells and whistles.
  """
  @moduledoc since: "0.0.1"

  @type tnode :: {any, any}
  @type mark :: any
  @type new :: any
  @type old :: any
  @type val :: any
  @type elem :: any

  @doc"""
  Creates a new FIFO linked list
  """
  @spec new(any) :: tnode
  def new(elem\\nil)
  def new(elem) when is_nil(elem), do: {nil, nil}
  def new(elem), do: {elem, nil}

  @doc"""
  Inserts a new element before the given mark
  """
  @spec insert(tnode, mark, new) :: tnode
  def insert(list, mark, new)
  def insert({_, nil} = node, _mark, new), do: push(node, new)
  def insert({elem, _} = node, mark, new) when elem == mark, do: append(node, new)

  def insert({_, link} = node, mark, new) when elem(link, 0) != mark do
    put_elem(node, 1, insert(link, mark, new))
  end

  def insert({_, link} = node, mark, new) when elem(link, 0) == mark do
    put_elem(node, 1, {new, link})
  end

  @doc"""
  Inserts a new element to the beginning of the list
  """
  @spec append(tnode, elem) :: tnode
  def append(list, elem), do: {elem, list} 

  @doc"""
  Inserts a new element to the end of the list
  """
  @spec push(tnode, elem) :: tnode
  def push({_, nil} = node, elem), do: put_elem(node, 1, {elem, nil})
  def push({_, link} = node, elem), do: put_elem(node, 1, push(link, elem))

  @doc"""
  Delete a node from the linked list
  """
  @spec delete(tnode, old) :: tnode
  def delete({elem, link}, old) when elem == old, do: delete(link, old)

  def delete({_, link} = node, old) when elem(link, 0) != old do
    put_elem(node, 1, delete(link, old))
  end

  def delete({elem, nil} = node, old) when elem != old do
    node
  end

  def delete({_, link} = node, old) when elem(link, 0) == old do
    put_elem(node, 1, elem(link, 1))
  end

  def delete({_, link} = node, old) 
    when elem(link, 0) == old and 
    is_nil(elem(link, 1)) do
    put_elem(node, 1, nil)
  end

  @doc"""
  Update an existing node in the linked list
  """
  @spec update(tnode, old, new) :: tnode
  def update({elem, _link} = node, old, new) when elem == old do
    put_elem(node, 0, new)
  end

  def update({_, nil} = node, _old, _new), do: node

  def update({_, link} = node, old, new) when elem(link, 0) != old do 
    put_elem(node, 1, update(link, old, new))
  end

  def update({_, link} = node, old, new) when elem(link, 0) == old do
    new_link = put_elem(link, 0, new)
    put_elem(node, 1, update(new_link, old, new))
  end

  @doc"""
  Finds an element in the list
  """
  @spec find(tnode, val) :: tnode
  def find({elem, link}, val) when elem != val, do: find(link, val)
  def find({elem, _link} = node, val) when elem == val, do: node

  @doc"""
  Find the element located before the desired element in the list
  """
  @spec prev(tnode, val) :: tnode
  def prev({_elem, link}, val) when elem(link,0) != val, do: prev(link, val)
  def prev({_elem, link} = prevNode, val) when elem(link, 0) == val, do: prevNode

  @doc"""
  Locates the tail of the list and returns it
  """
  @spec tail(tnode) :: tnode
  def tail({_, nil} = node), do: node
  def tail({_, link}), do: tail(link)

  @doc"""
  Converts linked list to a list
  """
  @spec to_list(tnode, list) :: list
  def to_list(linked_list, list\\[])
  def to_list({elem, nil}, list), do: Enum.reverse([elem | list])
  def to_list({elem, link}, list), do: to_list(link, [elem | list])

  @doc"""
  Calculates the length of the fifo linked list
  """
  @spec len(tnode) :: integer
  def len({_, nil}), do: 1
  def len({_, link}), do: 1 + len(link)
end
